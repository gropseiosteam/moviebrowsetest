//
//  RestAPI.swift
//  MovieBrowserTest
//
//  Created by Mohd Arsad on 7/31/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import Foundation

//MARK: - URLs
struct APIConstant {
    
    static let API_Key = "21cb49b91dde1051d1ac7596b9a25a11"
    static let Poster_MainUrl = "http://image.tmdb.org/t/p/w185/"
    static let MainUrl = "https://api.themoviedb.org/3/movie/"
    
    static let MovieList           = APIConstant.MainUrl + "popular"
    static let MovieDetail        = APIConstant.MainUrl + ""
    
}

//MARK: - Response Model
//Supported Model for get API response
struct APIResponse<T: Codable>: Codable {
    
    let page: Int
    let total_results: Int
    let total_pages: Int
    let results: T
    
}

struct Movie: Codable {
    
    let id: Int
    let poster_path: String?
    let trailerKey: String?
    let video: Bool
    let original_title: String
    let overview: String
    let release_date: String
    let genres: String?
    
    init(movie: MovieDB) {
        self.id = Int(movie.id)
        self.poster_path = movie.posterPath
        self.video = movie.isVideo
        self.original_title = movie.title ?? ""
        self.overview = movie.overview ?? ""
        self.release_date = movie.releaseDate ?? ""
        self.genres = movie.genres ?? ""
        self.trailerKey = movie.trailerKey ?? ""
    }
}
struct MovieDetail: Codable {
    struct Genres: Codable {
        let id: Int
        let name: String
    }
    struct Video: Codable {
        let key: String
    }
    struct Result: Codable {
        let results: [Video]?
    }
    let genres: [Genres]
    let videos: Result?
}

//MARK: - RestAPI Class
open class RestAPI {
    
    static let shared = RestAPI()
    
    /// Get Movie List From Server
    ///
    /// - Parameters:
    ///   - params: params for getting movies
    ///   - completion: response from server
    func movieList(completion: @escaping([Movie]?) -> ()) {
        
        let urlString = APIConstant.MovieList + "?api_key=\(APIConstant.API_Key)"
        if let url = URL(string: urlString) {
            var urlRequest: URLRequest = URLRequest(url: url)
            urlRequest.httpMethod = "GET"
            let session = URLSession.shared
            session.dataTask(with: urlRequest) { (data, response, error) in
                guard let data = data else {
                    completion(nil)
                    return
                }
                print(String(data: data, encoding: .utf8) ?? "No Data Found")
                do {
                    let serverData = try JSONDecoder().decode(APIResponse<[Movie]>.self, from: data)
                    completion(serverData.results)
                    return
                } catch {
                    completion(nil)
                    return
                }
            }.resume()
        }
    }
    
    /// Get Movie Details From Server
    ///
    /// - Parameters:
    ///   - params: params for getting movies
    ///   - completion: response from server
    func movieDetails(movieId: String, completion: @escaping(MovieDetail?) -> ()) {
        
        let urlString = APIConstant.MovieDetail + "\(movieId)" + "?api_key=\(APIConstant.API_Key)&append_to_response=videos"
        if let url = URL(string: urlString) {
            var urlRequest: URLRequest = URLRequest(url: url)
            urlRequest.httpMethod = "GET"
            let session = URLSession.shared
            session.dataTask(with: urlRequest) { (data, response, error) in
                guard let data = data else {
                    completion(nil)
                    return
                }
                print(String(data: data, encoding: .utf8) ?? "No Data Found")
                do {
                    let serverData = try JSONDecoder().decode(MovieDetail.self, from: data)
                    completion(serverData)
                    return
                } catch {
                    print(error)
                    completion(nil)
                    return
                }
            }.resume()
        }
    }
}
