//
//  Sync.swift
//  MovieBrowserTest
//
//  Created by Mohd Arsad on 7/31/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import Foundation
import UIKit
import CoreData

//MARK: - RestAPI Class
open class Sync {
    
    struct Table {
        static let Movie = "MovieDB"
    }
    private var context: NSManagedObjectContext {
        get {
            return (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        }
    }
    
    static let instance = Sync()
    
    /// Insert Movies in Local DB
    ///
    /// - Parameter movies: movies list
    func insertMovies(movies: [Movie]) {
        let context = Sync.instance.context
        for movie in movies {
            if !isExist(movie: movie) {
                let entity = NSEntityDescription.entity(forEntityName: Sync.Table.Movie, in: context)
                let movieDB = NSManagedObject(entity: entity!, insertInto: context) as? MovieDB
                movieDB?.id = Int32(movie.id)
                movieDB?.title = movie.original_title
                movieDB?.posterPath = movie.poster_path ?? ""
                movieDB?.genres = ""
                movieDB?.overview = movie.overview
                movieDB?.isVideo = movie.video
                movieDB?.releaseDate = movie.release_date
                movieDB?.trailerKey = ""
                do {
                    try context.save()
                    print("Movie Saved")
                } catch {
                    print("Failed saving")
                }
            }
        }
    }
    /// Update Genres Detail into DB
    ///
    /// - Parameters:
    ///   - genres: String
    ///   - movie: Movie Model
    func updateMovie(genres: String,trailerKey: String, in movie: Movie) {
        
        let context = Sync.instance.context
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Sync.Table.Movie)
        request.predicate = NSPredicate(format: "id = %@", "\(movie.id)")
        request.returnsObjectsAsFaults = false
        do {
            if let result = try context.fetch(request) as? [MovieDB] {
                result.first?.genres = genres
                result.first?.trailerKey = trailerKey
                do {
                    try context.save()
                    print("Genres Updated")
                } catch {
                    print("Failed Updating")
                }
            } else {
                print("Movie not found!")
            }
        } catch {
            print("Failed: \(error)")
        }
    }
    
    /// Get Movies from Local DB
    ///
    /// - Parameter completion: movies list
    func getMovies(completion: @escaping([Movie]?) -> ()) {
        
        let context = Sync.instance.context
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Sync.Table.Movie)
        request.returnsObjectsAsFaults = false
        do {
            if let result = try context.fetch(request) as? [MovieDB] {
                let movies = result.map({ Movie.init(movie: $0) })
                completion(movies)
            } else {
                completion([])
            }
        } catch {
            completion([])
        }
    }
}
// MARK: - Private Methods
extension Sync {
    /// Check Movie is Already Exist
    ///
    /// - Parameter movie: movie Object
    /// - Returns: Bool for success/error
    private func isExist(movie: Movie) -> Bool {
        let context = Sync.instance.context
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Sync.Table.Movie)
        request.predicate = NSPredicate(format: "id = %@", "\(movie.id)")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            return (result.count > 0) ? true : false
        } catch {
            return false
        }
    }
}
