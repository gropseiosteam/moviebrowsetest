//
//  ViewController.swift
//  MovieBrowserTest
//
//  Created by Mohd Arsad on 7/31/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class MovieDetailVC: UIViewController {
    
    @IBOutlet weak var posterIV: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var watchTrailerBtn: UIButton!
    @IBOutlet weak var genresLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var overviewLbl: UILabel!
    
    var selectedMovie: Movie?
    private var trailerKey: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateMovieDetails()
    }
    
    @IBAction func onClickWatchTrailerBtn(_ sender: Any) {
        guard let trailerKey = self.trailerKey, trailerKey.count > 0 else {
            return
        }
        print("TrailerKey: \(trailerKey)")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "YoutubePlayerVC") as! YoutubePlayerVC
        vc.videoKey = trailerKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    private func updateMovieDetails() {
        guard let movie = selectedMovie else { return }
        self.posterIV.imageFromServerURL((APIConstant.Poster_MainUrl + (movie.poster_path ?? "")), placeHolder: nil)
        self.nameLbl.text = movie.original_title
        self.dateLbl.text = movie.release_date
        self.genresLbl.text = movie.genres ?? ""
        self.overviewLbl.text = movie.overview
        self.trailerKey = movie.trailerKey
        if (genresLbl.text?.isEmpty)! {
            self.getMovieDetailsFromServer()
        }
    }
}

// MARK: - APIs Method
extension MovieDetailVC {
    private func getMovieDetailsFromServer() {
        guard let movie = selectedMovie else { return }
        
        DispatchQueue.global(qos: .background).async {
            RestAPI.shared.movieDetails(movieId: "\(movie.id)", completion: { (movieDetail) in
                guard let movieDetail = movieDetail else { return }
                let genresStr: String = movieDetail.genres.map({ return $0.name }).joined(separator: ",")
                DispatchQueue.main.async {
                    self.genresLbl.text = genresStr
                }
                let trailerKey: String = movieDetail.videos?.results?.last?.key ?? ""
                self.trailerKey = trailerKey
                Sync.instance.updateMovie(genres: genresStr, trailerKey: trailerKey, in: movie)
            })
        }
    }
}
