//
//  MovieTblCell.swift
//  MovieBrowserTest
//
//  Created by Mohd Arsad on 7/31/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class MovieTblCell: UITableViewCell {
    
    @IBOutlet weak var bannerIV: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
    var movie: Movie? {
        didSet {
            updateMovieDetails()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func updateMovieDetails() {
        self.nameLbl.text = self.movie?.original_title ?? ""
        self.bannerIV.imageFromServerURL((APIConstant.Poster_MainUrl + (self.movie?.poster_path ?? "")), placeHolder: nil)
    }
}
