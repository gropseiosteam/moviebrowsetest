//
//  CatelogVC.swift
//  MovieBrowserTest
//
//  Created by Mohd Arsad on 7/31/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class CatelogVC: UIViewController, UISearchBarDelegate {
    
    //MARK: - All Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var movies: [Movie] = []
    private var movieServerList: [Movie] = []
    
    //MARK: - Start
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.searchBar.delegate = self
        self.setupKeyboard()

        self.refreshMovies()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getMoviesFromServer()
    }
    
    //Suport Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text!.isEmpty {
            self.movies = self.movieServerList
        } else {
            self.movies = self.movieServerList.filter({ (movie) -> Bool in
                return movie.original_title.lowercased().contains(searchBar.text!.lowercased())
            })
        }
        self.tableView.reloadData()
        searchBar.resignFirstResponder()
    }
    private func setupKeyboard() {
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.searchBar.frame.origin.y -= keyboardSize.height
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.searchBar.frame.origin.y += keyboardSize.height
        }
    }
}
// MARK: - UITableViewDelegate, UITableViewDataSource
extension CatelogVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? MovieTblCell
        cell?.movie = movies[indexPath.row]
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailVC") as! MovieDetailVC
        vc.selectedMovie = movies[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - APIs Method
extension CatelogVC {
    private func refreshMovies() {
        Sync.instance.getMovies { (movies) in
            guard let movies = movies else { return }
            DispatchQueue.main.async {
                self.movieServerList = movies
                self.movies = movies
                self.tableView.reloadData()
            }
        }
    }
    private func getMoviesFromServer() {
        DispatchQueue.global(qos: .background).async {
            RestAPI.shared.movieList(completion: { (movies) in
                guard let movies = movies else { return }
                Sync.instance.insertMovies(movies: movies)
                self.refreshMovies()
            })
        }
    }
}
