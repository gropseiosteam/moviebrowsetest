//
//  YoutubePlayerVC.swift
//  MovieBrowseTest
//
//  Created by Mohd Arsad on 04/08/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class YoutubePlayerVC: UIViewController {

    @IBOutlet weak var playerView: YTPlayerView!
    
    var videoKey: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if let videoId = videoKey {
            self.playerView.delegate = self
            let playerVars = ["controls": 1, "playsinline": 0, "autohide": 1, "showinfo": 1, "autoplay": 1, "modestbranding": 1]
            self.playerView.load(withVideoId: videoId, playerVars: playerVars)
        }
    }
}
extension YoutubePlayerVC: YTPlayerViewDelegate {
    func playerView(_ playerView: YTPlayerView, didPlayTime playTime: Float) {
        print("Did Play Time")
    }
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        print("Some Error Occurred")
    }
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerView.playVideo()
    }
    func playerView(_ playerView: YTPlayerView, didChangeTo quality: YTPlaybackQuality) {
        print("Change Quality")
    }
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        switch state {
        case .unstarted:
            break
        case .ended:
            self.navigationController?.popViewController(animated: true)
            break
        case .playing:
            break
        case .paused:
            break
        case .buffering:
            break
        case .queued:
            break
        case .unknown:
            break
        }
    }
}
